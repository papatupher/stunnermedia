from django.contrib import admin

from .models import *

class JobTitleAdmin(admin.ModelAdmin):
    class Meta:
        model = JobTitle

class JobOpeningAdmin(admin.ModelAdmin):
    class Meta:
        model = JobOpening

class ApplicantAdmin(admin.ModelAdmin):
    class Meta:
        model = Applicant

class EmailConfirmedAdmin(admin.ModelAdmin):
    class Meta:
        model = EmailConfirmed


admin.site.register(JobTitle)
admin.site.register(JobOpening)
admin.site.register(Applicant)
admin.site.register(EmailConfirmed)
