from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

class CareersConfig(AppConfig):
    name = 'careers'
    verbose_name = _('careers')

    def ready(self):
        import careers.signals
