
from django.forms import ModelForm
from django import forms
from .models import *


class ApplicantInitialForm(ModelForm):

	class Meta:
		model = Applicant
		fields = ('first_name', 'last_name', 'email', 'contact_number')

	def clean_email(self):
		email = self.cleaned_data.get("email")
		applicants = Applicant.objects.filter(email=email).count()
		print(applicants)
		if applicants > 0:
			raise forms.ValidationError("This email has already been registter.")
		return email

	def save(self, commit=True):
		applicant = super(ApplicantInitialForm, self).save(commit=False)

		if commit:
			applicant.save()
		return applicant

class JobOpeningForm(ModelForm):
	class Meta:
		model = JobOpening
		include = []
		fields = [
			'image',
			'position',
			'overview',
			'details',
			'vacancy',
			'initial_exam'
		]

		# widgets = {
        #     'overview': forms.CharField(widget=RichTextWidget())
        # }

		labels = {
			'initial_exam': ('Initial Screening'),
		}
		def __init__(self,*args,**kwargs):
			super(JobOpeningForm,self ).__init__(*args,**kwargs)
			for field_name, field in self.fields.items():
				field.widget.attrs['class'] = 'form-control'

class JobOpeningFormEdit(JobOpeningForm):
    class Meta:
        model = JobOpening
        fields = [
			'image',
			'position',
			'overview',
			'details',
			'vacancy',
			'initial_exam',
			'is_active'
		]
