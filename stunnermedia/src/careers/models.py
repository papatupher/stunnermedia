from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import User
from django.conf import settings
from django.template.loader import render_to_string
from django.urls import reverse
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.

def job_opening_image_upload(instance, filename):
	return "image/job_openings/%s/%s" %(instance, filename)

class JobTitle(models.Model):
    title = models.CharField(max_length=150, null=False, blank=False, unique=True)
    description = models.TextField(null=True, blank=True)
    rank_and_file = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.title)

class JobOpening(models.Model):
    position = models.ForeignKey(JobTitle, null=False, blank=False, on_delete=models.DO_NOTHING)
    image = models.ImageField(upload_to=job_opening_image_upload, null=True, blank=True)
    overview = RichTextUploadingField(blank=False, null=False)
    details = RichTextUploadingField(blank=False, null=False)
    vacancy = models.IntegerField(null=True, blank=True)
    initial_exam = models.BooleanField(default=True)
    logs = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    start_date = models.DateTimeField(auto_now=True, auto_now_add=False)
    end_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return str(self.position)

    def get_unpublished(self):
        exclude_jt = JobOpening.objects.filter(is_active=False).values_list('position__id', flat=True)
        print("%s %s" %("JO:", exclude_jt))
        job_titles = JobTitle.objects.exclude(id__in=exclude_jt)
        return job_titles

class Applicant(models.Model):
    email = models.EmailField(null=False, unique=True)
    first_name = models.CharField(max_length=50, null=False)
    last_name = models.CharField(max_length=50, null=False)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{7,12}$', message="landline or mobile")
    contact_number = models.CharField(validators=[phone_regex], max_length=17, blank=False, null=False) # validators should be a list
    desired_position = models.CharField(max_length=80, null=False)

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name)

class EmailConfirmed(models.Model):
	applicant = models.OneToOneField(Applicant,on_delete=models.CASCADE)
	activation_key = models.CharField(max_length=200)
	confirmed = models.BooleanField(default=False)

	def __unicode__(self):
		return str(self.confirmed)

	def activate_applicant_email(self):
		activation_url = "%s%s" %(settings.SITE_URL, reverse("activation-view", args=[self.activation_key]))
		context ={
			"activation_key": self.activation_key,
			"activation_url": activation_url,
			"applicant": "%s %s" %(self.applicant.first_name, self.applicant.last_name)
		}
		message = render_to_string("app/activation.txt", context)
		subject = "Activate your Email"
		self.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)

	def email_user(self, subject, message, from_email=None, **kwargs):
		send_mail(subject, message, from_email, [self.applicant.email], kwargs)
