from django.shortcuts import render, HttpResponseRedirect, Http404
from django.core.paginator import Paginator
from .models import EmailConfirmed, JobOpening
from .forms import ApplicantInitialForm
import re
from django.shortcuts import get_object_or_404


# Create your views here.

def careers(request):
	opportunities = JobOpening.objects.filter(is_active=True)
	context = {
    	'opportunities':opportunities
    }
	return render(request, 'app/careers.html', context=context)

def career_details(request,id):
	opportunity = get_object_or_404(JobOpening,id=id)
	context = {
		'opportunity':opportunity
	}
	return render(request, 'app/career-details.html', context=context)


def application_form(request):
    form = ApplicantInitialForm(request.POST or None)

    if form.is_valid():
        print("FORM IS VALID!!!")
        applicant = form.save(commit=False)

        applicant.save()
        return HttpResponseRedirect("/")

    context = {
        "form": form,
        }
    print("FUCK")
    return render(request, 'app/career-details.html', context=context)

SHA1_RE = re.compile('^[a-f0-9]{40}$')

def activation_view(request, activation_key):
	if SHA1_RE.search(activation_key):
		print("activation key is real")
		try:
			instance = EmailConfirmed.objects.get(activation_key=activation_key)
		except EmailConfirmed.DoesNotExist:
			instance = None
			return HttpResponseRedirect("/")

		if instance is not None and not instance.confirmed:
			instance.confirmed = True
			instance.activation_key = "Confirmed"
			instance.save()

		elif instance is not None and instance.confirmed:
			print("already registered")


		context = {"message": "TEST"}
		return render(request, "app/career-details.html", context)
	else:
		raise Http404
