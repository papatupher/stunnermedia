import random, hashlib
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import post_save

from django.dispatch import receiver

from careers.models import Applicant, EmailConfirmed

@receiver(post_save, sender=Applicant)
def application_created(sender, instance, created, **kwargs):
	print("signals application_created")
	applicant = instance

	if created:
		email_confirmed, email_is_created = EmailConfirmed.objects.get_or_create(applicant=applicant)
		if email_is_created:
			short_hash = hashlib.sha1(str(random.random()).encode('utf-8')).hexdigest()[:5]
			base, domain = str(applicant.email).split("@")
			activation_key = hashlib.sha1(str(short_hash+base).encode('utf-8')).hexdigest()
			email_confirmed.activation_key = activation_key
			email_confirmed.save()
			email_confirmed.activate_applicant_email()


# post_save.connect(application_created, sender=Applicant)
