from django.conf import settings
from django.urls import path, re_path

from django.contrib import admin
from .views import *
admin.autodiscover()

urlpatterns = [
   path('', careers, name='careers'),
   path('details/<int:id>', career_details, name='career_details'),
   path('application-form', application_form, name='application-form'),
   path('application/activate/<int:id>', activation_view, name='activation-view'),
]
