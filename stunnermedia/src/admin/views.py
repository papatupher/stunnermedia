from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect

import json


def admin_home(request):

    context = {

    }

    return render(request, 'admin/base/base.html', context=context)
