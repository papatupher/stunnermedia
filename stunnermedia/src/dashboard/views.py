from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect

from careers.forms import *
from careers.models import *
from django.shortcuts import get_object_or_404
from django.contrib.auth import logout, login, authenticate
import json

def logout_view(request):

	logout(request)
	return HttpResponseRedirect('%s'%(reverse("auth_login")))

def login_view(request):

    if request.method == 'POST':
        username =request.POST["loginUsername"]
        password =request.POST["loginPassword"]
        user = authenticate(username=username, password=password)
        print(user)
        if user:
            login(request, user)
            return HttpResponseRedirect('%s'%(reverse("admin_home")))

    context = {}
    return render(request, "admin/base/login.html", context)

def admin_home(request):

    context = {
        'page_header':'Dashboard'
    }

    return render(request, 'admin/base/base.html', context=context)


def careers_opportunity(request):
    form = JobOpeningForm(request.POST or None, request.FILES or None)
    job_openings = JobOpening.objects.filter(is_active=True)
    context = {
        'form':form,
        'jds':job_openings,
    }

    return render(request, 'admin/careers/opportunity.html', context=context)

def careers_opportunity_form(request, id=None):
    if request.method == 'POST':
        if id:
            instance = get_object_or_404(JobOpening, id=id)
            form = JobOpeningFormEdit(request.POST or None, request.FILES or None, instance=instance)
        else:
            form = JobOpeningForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            form.save()
            pass
    else:
        if id:
            instance = get_object_or_404(JobOpening, id=id)
            form = JobOpeningFormEdit(request.POST or None, request.FILES or None, instance=instance)
        else:
            form = JobOpeningForm()
    context = {
        'form':form,
    }

    return render(request, 'admin/careers/opportunity_create.html', context=context)
