from django.conf import settings
from django.urls import path, re_path

from django.contrib import admin
from .views import *
admin.autodiscover()

urlpatterns = [
   path('logout/',logout_view, name='auth_logout'),
   path('login/', login_view, name='auth_login'),
   path('', admin_home, name='admin_home'),
   path('careers/opportunity', careers_opportunity, name='careers_opportunity'),
   path('careers/opportunity/create', careers_opportunity_form, name='careers_opportunity_create'),
   path('careers/opportunity/edit/<int:id>', careers_opportunity_form, name='careers_opportunity_edit'),
]
