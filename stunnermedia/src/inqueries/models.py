from django.db import models
from django.utils import timezone

class Contact(models.Model):
	name = models.CharField(max_length=120)
	email = models.EmailField(max_length=120, null=False)
	message = models.TextField(null=False)
	is_read = models.BooleanField(default=False)

	date_submitted = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.email

	def recently_received(self):
		return self.date_submitted >= timezone.now() - datetime.timedelta(days=1)
	recently_received.boolean = True
