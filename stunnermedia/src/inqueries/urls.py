from django.conf import settings
from django.urls import path, re_path

from django.contrib import admin
from .views import *
admin.autodiscover()

urlpatterns = [
   re_path(r'^$', ajax_send_email, name="send_mail"),

]
