from django.db import models
from careers.models import JobTitle

from djrichtextfield.models import RichTextField

ANSWER_TYPE = (
	('fill_in_the_blank', 'Fill in the blank'),
	('multiple_choice', 'Multiple choice'),
	)

def creative_exam_upload(instance, filename):
	return "image/creativity_exam/%s/%s" %(instance, filename)

class Exam(models.Model):
    position = models.ForeignKey(JobTitle, null=False, blank=False, on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=150, null=False, blank=False)
    duration = models.DateTimeField(auto_now=True, auto_now_add=False)
    timer = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.position)

class Logic(models.Model):
    exam = models.ForeignKey(Exam, null=False, blank=False, on_delete=models.DO_NOTHING)
    question = RichTextField(blank=False, null=False)
    type = models.CharField(max_length=50, choices=ANSWER_TYPE, default='fill_in_the_blank')
    choices = models.CharField(max_length=500, blank=True, null=True)
    answer = models.CharField(max_length=500, blank=False, null=False)

    def __str__(self):
        return str(self.exam)

class Creative(models.Model):
    exam = models.ForeignKey(Exam, null=False, blank=False, on_delete=models.DO_NOTHING)
    image = models.ImageField(upload_to=creative_exam_upload, null=False, blank=False)

    def __str__(self):
        return str(self.title)

# class ExamResult(models.Model):
#     exam = models.ForeignKey(JobTitle, null=False, blank=False, on_delete=models.DO_NOTHING)
