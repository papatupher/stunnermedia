      //Dweller Global//
  var dweller={};
  
  //Particle explosion function//
  dweller.moveParticles = function(obj){
 
        var containerDiv = obj.containerDiv;
        var containerPos = obj.containerPos;
        var offSetX = obj.offSetX;
        var offSetY = obj.offSetY;
        var radius = obj.radius;
        var numberOfPar = obj.numberOfPar;

        $("#"+containerDiv).css({left:containerPos[0]+"px", top:containerPos[1]+"px"});

        for(var i=0; i<numberOfPar; i++){
            $("#"+containerDiv+"").append("<img class='abs' src='http://test.thebasement.tv/codepen/ex2/particle.png'>");
        }

        var containerLen = $("#"+containerDiv).children().length;
        var imgArray = [];
        var distance = 360/containerLen;

        var rangeArray=[];

        for (var i=0; i<containerLen; i++){
            imgArray.push($("#"+containerDiv).children().eq(i));
            var v = distance*i;
            rangeArray.push(v);
        }

        for(var i=0; i<imgArray.length; i++){
            
            var left = offSetX+Math.sin(rangeArray[i]* ( Math.PI/180))*Math.floor((Math.random()*radius[1])+radius[0]);
            var top = offSetX+Math.cos(rangeArray[i]* ( Math.PI/180))*Math.floor((Math.random()*radius[1])+radius[0]);

            $(imgArray[i]).css({"left":offSetX+"px", "top":offSetY+"px"});

            TweenMax.to(imgArray[i], 0.8, {"left":left, "top":top, opacity:0, ease:Power2.easeOut});

        }
    }


  //Elements floating around the Logo, animation in and animation out//
    dweller.elements =
    {
        items:[],

        animateIn : function(){
            var tl = new TimelineMax();
            for(var i=0; i<this.items.length; i++)
            {
                var overlap="-=0.75";
                if(i==0)
                {
                    overlap="0.5";
                }

                tl.to(this.items[i],0.8,{left:this.items[i].attr('x'), top:this.items[i].attr('y'), opacity:1, ease:Back.easeOut}, overlap);
        
            }
            return tl;
        },

        animateOut : function()
        {
            var tl = new TimelineMax();
            for(var i=0; i<this.items.length; i++)
            {
                tl.to(this.items[i],0.8,{top:380,left:540, opacity:0, ease:Back.easeInOut}, "-=0.78");
            }
            return tl;
        },

        populateDefault: function()
        {
            var totalLength = $("#elements").children().length;
            for(var i=0; i<totalLength; i++)
            {
                $("#elements").children().eq(i).attr('name','element'+i);
                this.items.push($("#elements").children().eq(i));
                TweenMax.set(this.items[i],{top:380,left:540, opacity:0});
            }
        }
    }
    

  //Dweller Logo animation in and animation out//
    dweller.logoSection = {
        dweller : $("#logo"),
        logoT1 : $("#logoText1"),
        logoT2 : $("#logoText2"),

        animateIn : function()
        {
            var tl = new TimelineMax();
                tl.fromTo(this.dweller,1,{opacity:0, backgroundPositionY:'-60px'},{opacity:1, backgroundPositionY:'20px', ease:Elastic.easeInOut},"0.1");
                tl.fromTo(this.logoT2,0.5,{opacity:0, backgroundPositionY:'-49px'},{opacity:1, backgroundPositionY:'0px', ease:Power4.easeOut}, "-=0.4");
                tl.fromTo(this.logoT1,0.5,{opacity:0, backgroundPositionY:'-25px'},{opacity:1, backgroundPositionY:'0px', ease:Power4.easeOut}, "-=0.3");
            return tl;

        },

        animateOut : function(){
            var tl = new TimelineMax();
                tl.to([this.dweller,this.logoT1,this.logoT2],0.2, {opacity:0});

            return tl;
        },

        populateDefault: function()
        {
            TweenMax.set(this.dweller,{top:this.dweller.attr('y'), left:this.dweller.attr('x'),opacity:0});
            TweenMax.set(this.logoT1,{top:this.logoT1.attr('y'), left:this.logoT1.attr('x'),opacity:0});
            TweenMax.set(this.logoT2,{top:this.logoT2.attr('y'), left:this.logoT2.attr('x'),opacity:0});
        }

    }

    
  //Setting Defaults//
    dweller.elements.populateDefault();
    dweller.logoSection.populateDefault();

  
  //Master Time Line
    var masterTL = new TimelineMax();
    masterTL.add(dweller.logoSection.animateIn());
    masterTL.add(dweller.elements.animateIn());
    masterTL.add(dweller.logoSection.animateOut(),"-=0.2");

    masterTL.call(dweller.moveParticles,[{containerDiv:'particles',containerPos:[540,413],offSetX:0,offSetY:0,radius:[40,340],numberOfPar:30}]);

    masterTL.add(dweller.elements.animateOut(),"-=0.5");

    // masterTL.add(TweenMax.to("body",0.2,{backgroundColor:"#000000"}),"-=0.42");
    masterTL.add(TweenMax.to("#whiteLogo",0.7,{visibility:'visible', opacity:1, left:"+=8px", top:"+=8px", width:145, height:145}),"-=0.35");
    masterTL.add(TweenMax.to("#planeGroup",0.9,{opacity:0.2, width:260, height:197, left:"630px",top:"140px",visibility:'visible',ease:Power2.easeOut}),"-=0.7");
    masterTL.add(TweenMax.to("#whiteLogo",0.2,{opacity:0}),"-=0.35");
    masterTL.add(TweenMax.to("#planeGroup",0.2,{opacity:0}),"-=0.35");

    // masterTL.add(TweenMax.to("body",0.2,{backgroundColor:"#f9ea01"}),"-=0.3");
    masterTL.add(dweller.logoSection.animateIn(),"-=0.65");
    masterTL.eventCallback("onComplete", function(){
            
            if($('#ftco-loader').length > 0) {
                window.scrollTo(0, 0);
                $('#ftco-loader').removeClass('show');
            }
    });


    // masterTL.seek(3);
    masterTL.pause();
  