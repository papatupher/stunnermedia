$(document).ready(function() {

    (function($, window, document, undefined) {

    	
	
		$(document).on('click', '.nav-item', function(){
			
			$('.nav-content').addClass('hidden');
			var section = $(this).attr('data-target');
			$(section).removeClass('hidden');
		});

    });
});


$(window).on('load', function() {

 	window.scrollTo(0, 0);

	var tl = new TimelineMax();
	tl.to("#preloader-circle", 0.5, { display: "none" });
	tl.fromTo('#stunner-intro-text', 1, { display: "none" }, { display: "flex"});
	tl.from("#line", 0.5, { scaleX: 0, transformOrigin: "right center" });
	tl.from("#upper", 0.75, { y: 70 }, "text");
	tl.from("#lower", 0.75, { y: -60 }, "text");
	tl.to("#line, #upper, #lower", 1, { opacity: 0 }, "+=3");
	tl.eventCallback("onComplete", function(){
		 masterTL.play();
	});	
});



if($('.grid').length > 0){

	// init Masonry
	var $grid = $('.grid').masonry({
	  itemSelector: '.grid-item',
	  percentPosition: true,
	  columnWidth: '.grid-sizer'
	});

	// layout Masonry after each image loads
	// $grid.imagesLoaded().progress( function() {
	//   $grid.masonry();
	// });  
}

